import { useState, useEffect, useContext } from 'react'
import { Form, Button } from 'react-bootstrap'
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register() {

	const { user } = useContext(UserContext)

	const navigate = useNavigate()

	/*State hooks to store the values of the input fields*/
	const [firstName, setFirstName] = useState("")
	const [lastName, setLastName] = useState("")
	const [email, setEmail] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");
	/*State to determine whether the submit button is enabled or not*/
	const [isActive, setIsActive] = useState(false)

	// console.log(email);
	// console.log(password1);
	// console.log(password2);

	function registerUser(e) {

		e.preventDefault()

		fetch(`http://localhost:4000/users/checkEmail`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if (data === true) {

		  Swal.fire({
              title: "Email already registered",
              icon: "error",
              text: "Kindly provide a different email to complete the registration"
                })
			} else {

				fetch(`http://localhost:4000/users/register`, {
					method: "POST",
					headers: {
						'Content-Type': "application/json"
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						password: password1
					})
				})

				.then(res => res.json())
				.then(data => {
					console.log(data)

					if (data === true) {

						Swal.fire({
	              title: "Registration Successful",
	              icon: "success",
	              text: "Welcome to Zuitt"
          })

						setFirstName("");
						setLastName("");
						setEmail("");
						setMobileNo("");
						setPassword1("");
						setPassword2("");

						navigate("/login")

					} else {

							Swal.fire({
	              title: "Something went wrong!",
	              icon: "error",
	              text: "Please try again!"
          })

					}
				})

		  		
				}
		})

		setFirstName("");
		setLastName("");
		setEmail("");
		setMobileNo("");
		setPassword1("");
		setPassword2("");



	}

	useEffect(() => {

		if((firstName !== "" && lastName !== "" && mobileNo.length === 11 && email !== "" && password1 !== "" && password2 !== "") && (password1 === password2)) {

			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [firstName, lastName, email, mobileNo, password1, password2])

	return(


    (user.id !== null) ?
        <Navigate to="/courses" />

        :

	<Form className="mt-3" onSubmit={(e)=> registerUser(e)}>
	  <h1 className="text-center">Register</h1>

	  <Form.Group className="mb-3" controlId="firstName">
        <Form.Label>First Name</Form.Label>
        <Form.Control
         	type="text" 
         	placeholder="Enter First Name"
         	value={firstName}
         	onChange={e => setFirstName(e.target.value)}
         	required />
      </Form.Group>

       <Form.Group className="mb-3" controlId="lastName">
        <Form.Label>Last Name</Form.Label>
        <Form.Control
         	type="text" 
         	placeholder="Enter Last Name"
         	value={lastName}
         	onChange={e => setLastName(e.target.value)}
         	required />
      </Form.Group>

      <Form.Group className="mb-3" controlId="userEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control 
       	 	type="email" 
       		 placeholder="Enter email" 
        	value={email}
        	onChange={e =>setEmail(e.target.value)}
        	required/>
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>


       <Form.Group className="mb-3" controlId="mobileNo">
        <Form.Label>Mobile Number</Form.Label>
        <Form.Control
         	type="text" 
         	placeholder="Mobile Number"
         	value={mobileNo}
         	onChange={e => setMobileNo(e.target.value)}
         	required />
      </Form.Group>

      <Form.Group className="mb-3" controlId="password1">
        <Form.Label>Password</Form.Label>
        <Form.Control
         	type="password" 
         	placeholder="Password"
         	value={password1}
         	onChange={e => setPassword1(e.target.value)}
         	required />
      </Form.Group>

      <Form.Group className="mb-3" controlId="password2">
        <Form.Label>Verify Password</Form.Label>
        <Form.Control
         	type="password" 
         	placeholder="Password"
         	value={password2}
         	onChange={e => setPassword2(e.target.value)}
         	required />
      </Form.Group>

      {
		isActive ?
		<Button variant="primary" type="submit" id="submitBtn">
        Submit
      	</Button>
      	:
       <Button variant="secondary" type="submit" id="submitBtn">
        Submit
       </Button>
      }

    </Form>


		)
}