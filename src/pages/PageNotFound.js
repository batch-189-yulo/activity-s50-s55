import {Row, Col, Button} from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function PageNotFound () {


	return(

		<Row>
			<Col className="p-5">
				<h1>PageNotFound</h1>	
				<p>ERROR 404.</p>
				<Link as={ Link } to="/">Return to Home page</Link>
			</Col>
		</Row>

		)
}