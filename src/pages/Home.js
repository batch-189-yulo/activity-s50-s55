import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
// import CourseCard from '../components/courseCard'

export default function Home() {

	return(
			<>
				<Banner/>
				<Highlights/>
				{/*<CourseCard/>*/}
			</>

		)
}