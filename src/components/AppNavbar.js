// import Navbar from 'react-bootstrap/Navbar';
// import Nav from 'react-bootstrap/Nav';
// import Container from 'react-bootstrap/Container';


import {Fragment, useContext} from 'react';
import { Navbar, Nav, Container } from 'react-bootstrap'
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';


export default function AppNavbar() {

  const { user } = useContext(UserContext)

  // State to store the user information stored in the login page
  // const [user, setUser] = useState(localStorage.getItem("email"));
  /*
    getItem() method that returns value of a specified object item
  
    Syntax:
        localStorage.getItem("propertyName")
  */

  console.log(user);

	return (
		<Navbar bg="dark" expand="lg" variant="dark">
      <Container>
        <Navbar.Brand as={ Link } to="/">Batch 189</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link as={ Link } to="/">Home</Nav.Link>
            <Nav.Link as={ Link } to="/courses">Courses</Nav.Link>
           {
            (user.id !== null) ?
                  <Nav.Link as={ Link } to="/logout">Logout</Nav.Link>
                  :
                  <Fragment>
                    <Nav.Link as={ Link } to="/login">Login</Nav.Link>
                    <Nav.Link as={ Link } to="/register">Register</Nav.Link>
                  </Fragment>
           }
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>

		)
}