// import { useState,useEffect } from 'react'
import { Row, Col, Card, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'

export default function CourseCard({courseProp}) {

// console.log(props)
// console.log(typeof props)
// console.log(props.courseProp.name)
// console.log(courseProp)

/*
User the state hook for this component to be able to store its state
States are used to keep track of information related to individual components

Syntax:
	const [getter, setter] = useState(initialGetterValue)
*/

// const [count, setCount] = useState(0);
// const [seats, setSeats] = useState(30);

//     function enroll() {
//        // if (seats > 0) {
//             setCount(count + 1);
//             console.log('Enrollees: ' + count);
//             setSeats(seats - 1);
//             console.log('Seats: ' + seats);
//        //   } else {
//        //      alert("No more seats available");
//        // }; 

//     }

// // function enroll() {

// 	// if(count === 30 && seat === 0){
// 	// 	alert(`No more seats available for the ${name} package.`)
// 	// 	return
// 	// }

// // 	setCount(count + 1)
// // 	setSeat(seat - 1)
// // }

// useEffect(() => {
// 	if (seats === 0) {
// 		alert('No more seats available')
// 	}
// }, [seats])

// Deconstruct the course proprerties into their own variables.
const { name, description, price, _id } = courseProp;

return(
	<Row>
		<Col>
			<Card>     
				<Card.Body>
					<Card.Title>{name}</Card.Title>
					<Card.Subtitle>Description:</Card.Subtitle>
					<Card.Text>{description}</Card.Text>
					<Card.Subtitle className="text-primary">Price:</Card.Subtitle>
					<Card.Text>Php {price}</Card.Text>
					<Button variant="primary" as={Link} to={`/courses/${_id}`}>Details</Button>
				</Card.Body>
			 </Card>
		</Col>
	</Row>
	)
}

